## Product 1

#### Product name
MAXQ AI - Interpret medical images for medical diagnostic

#### Product link
[link](https://www.maxq.ai//)  

#### Product Short description
* They have developed software that uses artificial intelligence to interpret medical images such as non-contrast head computed tomography (CT) scans and surrounding patient data, for use in acute care settings. 
* AI solutions are well-suited to help acute care physicians who are under extreme pressure to make quick and accurate decisions while treating a large number of patients.

#### Product is combination of features
 * Deep learning  
 * Machine vision  
 * Artificial intelligence  

#### Product is provided by which company?
**MAXQ AI**, an AI-Augmented healthcare company           


## Product 2

#### Product name
Scandit - ID Scanning Software for Smart Devices  

#### Product link
[link](https://www.scandit.com/products/id-scanning/)  

#### Product Short description
* Scandit brings high-performance ID scanning software into mobile apps, enabling smart devices with a camera to capture data from different types of identity documents.
* Typical uses include passport and boarding pass checks at airports, ID checks or age verification when making a purchase or onboarding to a new employee or consumer app.

#### Product is combination of features
 * Image Recognition
 * Text Recognition
 * Face Recognition 
 

#### Product is provided by which company?
**Scandit**, ID Scanning Software  