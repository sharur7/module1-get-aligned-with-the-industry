# DNN Summary

### What is DNN?
* Deep Neural Networks (DNNs) are typically Feed Forward Networks (FFNNs) in which data flows from the input layer to the output layer without going backward and the links between the layers are one way which is in the forward direction and they never touch a node again. 

* The outputs are obtained by supervised learning with datasets of some information based on ‘what we want’ through back propagation. Like you go to a restaurant and the chef gives you an idea about the ingredients of your meal. FFNNs work in the same way as you will have the flavor of those specific ingredients while eating but just after finishing your meal you will forget what you have eaten. If the chef gives you the meal of same ingredients again you can’t recognize the ingredients, you have to start from scratch as you don’t have any memory of that. But the human brain doesn’t work like that.

<img src="https://gitlab.com/sharur7/module1-get-aligned-with-the-industry/-/raw/master/assignments/Deep-learning-diagram.png">

### How does DNN work?

Neural networks are comprised of layers of nodes, much like the human brain is made up of neurons. Nodes within individual layers are connected to adjacent layers. The network is said to be deeper based on the number of layers it has. A single neuron in the human brain receives thousands of signals from other neurons. In an artificial neural network, signals travel between nodes and assign corresponding weights. A heavier weighted node will exert more effect on the next layer of nodes. The final layer compiles the weighted inputs to produce an output. Deep learning systems require powerful hardware because they have a large amount of data being processed and involves several complex mathematical calculations. Even with such advanced hardware, however, deep learning training computations can take weeks.

### Important points to be noted:

* Deep Learning uses a Neural Network to imitate animal intelligence.
* There are three types of layers of neurons in a neural network: the Input Layer, the Hidden Layer(s), and the Output Layer.
* Connections between neurons are associated with a weight, dictating the importance of the input value.
* Neurons apply an Activation Function on the data to “standardize” the output coming out of the neuron.
* To train a Neural Network, you need a large data set.
* Iterating through the data set and comparing the outputs will produce a Cost Function, indicating how much the AI is off from the real outputs.
* After every iteration through the data set, the weights between neurons are adjusted using Gradient Descent to reduce the cost function.







