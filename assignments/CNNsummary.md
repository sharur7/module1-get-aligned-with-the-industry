# CNN Summary

### What is CNN?
A Convolutional Neural Network (ConvNet/CNN) is a Deep Learning algorithm which can take in an input image, assign importance (learnable weights and biases) to various aspects/objects in the image and be able to differentiate one from the other.<br> The pre-processing required in a ConvNet is much lower as compared to other classification algorithms.<br>

<img src ="https://gitlab.com/sharur7/module1-get-aligned-with-the-industry/-/raw/master/assignments/Cnn1.jpeg"><br>

There are various architectures of CNNs available which have been key in building algorithms which power and shall power AI as a whole in the foreseeable future. Some of them have been listed below:<br>
* LeNet
* AlexNet
* VGGNet
* GoogLeNet
* ResNet
* ZFNet

The primary tasks of convolutional neural networks are the following:

* Classify visual content (describe what they “see”)
* Recognize objects within is scenery (for example, eyes, nose, lips, ears on the face)
* Gather recognized objects into clusters (for example, eyes with eyes, noses with noses)

### How Does Convolutional Neural Network work?

* Convolutional layer - where the action starts. The convolutional layer is designed to identify the features of an image. Usually, it goes from the general (i.e., shapes) to specific (i.e., identifying elements of an object, the face of certain man, etc.).  
* Then goes Rectified Linear Unit layer (aka ReLu). This layer is an extension of a convolutional layer. The purpose of ReLu is to increase the non-linearity of the image. It is the process of stripping an image of excessive fat to provide a better feature extraction.
* Pooling layer is designed to reduce the number of parameters of the input i.e., perform regression. In other words, it concentrates on the meaty parts of the received information.
* The connected layer is a standard feed-forward neural network. It is a final straight line before the finish line where all the things are already evident. And it is only a matter of time when the results are confirmed.


